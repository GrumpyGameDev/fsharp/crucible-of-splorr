# 20201016 - Day 9

* a last attempt at reasonable DI
* "But I want easy answers that are universally agreed upon that will guarantee my software is considered cool." - Grumbledore
* "I want the perception of my code to be cool so that I by imputed coolness am considered cool" - Grumbledore

# 20201015 - Day 8

* monogame host

# 20201014 - Day 7

* guy on a grid and a backbuffer

# 20201013 - Day 6

* render a guy on a grid on the screen

# 20201012 - Day 5

* clearing with a color
* added instructions stub

# 20201011 - Day 4

* abstracting away ConsoleColor and ConsoleKey

# 20201010 - Day 3

* refactor console input/output

# 20201009 - Day 2

* foundations

# 20201008 - Day 1

* foundations