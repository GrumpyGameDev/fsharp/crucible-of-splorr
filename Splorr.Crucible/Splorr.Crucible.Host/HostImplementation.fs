﻿namespace Splorr.Crucible.Host

open Microsoft.Xna.Framework.Input
open Splorr.Crucible.Runner

module HostImplementation =
    let private cellWidth = 16
    let private cellHeight = 16

    let GetPreferredBackBufferSize () : int * int =
        (OutputImplementation.board.Columns * cellWidth, OutputImplementation.board.Rows * cellHeight)

    let GetTitle() : string =
        "Crucible of SPLORR!!"

    let GetGridSize() : int * int =
        (80,45)

    let GetKeyCommands() : Map<Keys, InputCommand> =
        Map.empty
        |> Map.add Keys.Left Left
        |> Map.add Keys.Right Right
        |> Map.add Keys.Down Down
        |> Map.add Keys.Up Up
        |> Map.add Keys.A Left
        |> Map.add Keys.D Right
        |> Map.add Keys.S Down
        |> Map.add Keys.W Up
        |> Map.add Keys.Z Red
        |> Map.add Keys.X Green
        |> Map.add Keys.C Blue
        |> Map.add Keys.E Yellow
        |> Map.add Keys.Escape Back
        |> Map.add Keys.F2 Start
        |> Map.add Keys.OemComma Previous
        |> Map.add Keys.OemPeriod Next

