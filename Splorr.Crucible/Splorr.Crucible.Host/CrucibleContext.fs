﻿namespace Splorr.Crucible.Host

open System

open Splorr.Crucible.Runner
open Splorr.Common
open Splorr.Common.Runner
open Splorr.Crucible.Business

type HostContext = 
    inherit CommonContext
    inherit Splorr.Common.Runner.RunnerContext<InputCommand>
    inherit Splorr.Crucible.Business.BusinessContext
    inherit Host.GetPreferredBackBufferSizeContext
    inherit Host.GetGridSizeContext
    inherit Host.GetTitleContext
    inherit Host.GetKeyCommandsContext

type CrucibleContext() =
    interface CommonContext
    interface RunnerContext<InputCommand>
    interface Input.PollForKeyContext<InputCommand> with
        member this.GetCommand = ref InputImplementation.Get
    interface Output.ClearContext with
        member this.PutClear = ref OutputImplementation.PutClear
    interface Output.SetBackgroundColorContext with
        member this.PutBackgroundHue = ref OutputImplementation.PutBackgroundColor
    interface Output.SetForegroundColorContext with
        member this.PutForegroundHue = ref OutputImplementation.PutForegroundColor
    interface Output.SetSizeContext with
        member this.PutSize = ref OutputImplementation.PutSize
    interface Output.SetPositionContext with
        member this.PutPosition = ref OutputImplementation.PutPosition
    interface Output.WriteTextContext with
        member this.PutText = ref OutputImplementation.PutWrite
    interface BusinessContext
    interface HostContext
    interface Host.GetPreferredBackBufferSizeContext with
        member this.GetPreferredBackBufferSize = ref HostImplementation.GetPreferredBackBufferSize
    interface Host.GetTitleContext with
        member this.GetTitle = ref HostImplementation.GetTitle
    interface Host.GetGridSizeContext with
        member this.GetGridSize = ref HostImplementation.GetGridSize
    interface Host.GetKeyCommandsContext with
        member this.GetKeyCommands = ref HostImplementation.GetKeyCommands
    interface AvatarIdentifier.GenerateContext with
        member this.GenerateAvatarIdentifier = ref (fun () -> Guid.NewGuid())
