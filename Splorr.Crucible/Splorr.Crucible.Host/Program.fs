﻿open System

open Splorr.Crucible.Runner
open Splorr.Crucible.Host
open Microsoft.Xna.Framework


[<EntryPoint>]
let main _ =
    let context = 
        CrucibleContext()
    use game = new HostGame(context)
    game.Run()
    0
