﻿namespace Splorr.Crucible.Host

open System
open Splorr.Common.Runner
open Splorr.Common

type internal BoardCell = 
    {
        glyph : byte
        foreground : Hue
        background : Hue
    }

module OutputImplementation =

    let private defaultBoardCell =
        {
            glyph = 0uy
            foreground = LightGray
            background = Black
        }
    let mutable private position : int * int = (0,0)
    let mutable private foreground : Hue = LightGray
    let mutable private background : Hue = Black
    let mutable internal board : Board<BoardCell> = Board<BoardCell>(1,1,defaultBoardCell)

    let PutSize
            (width: int, height : int)
            : unit =
        board <- Board<BoardCell>(width, height, defaultBoardCell)

    let PutPosition
            (column: int, row:int)
            : unit =
        position <- (column, row)

    let PutForegroundColor
            (color : Hue)
            : unit =
        foreground <- color

    
    let PutBackgroundColor
            (color : Hue)
            : unit =
        background <- color

    let PutClear
            ()
            : unit =
        let cell =
            {
                glyph = 0uy
                foreground = foreground
                background = background
            }
        [0..(board.Rows - 1)]
        |> List.iter
            (fun row ->
                [0..(board.Columns - 1)]
                |> List.iter
                    (fun column->
                        board.SetCell(column, row, cell)))

    let PutWrite
            (text:string)
            : unit =
        text.ToCharArray()
        |> Array.iter
            (fun glyph ->
                board.SetCell
                    (position 
                    |> fst, 
                    position 
                    |> snd, 
                    {
                        glyph = (byte)glyph 
                        foreground = foreground
                        background = background
                    })
                position <- ((position |> fst) + 1, position |> snd))



