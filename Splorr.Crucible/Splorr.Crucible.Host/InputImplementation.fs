﻿namespace Splorr.Crucible.Host

open System
open Splorr.Crucible.Runner

module InputImplementation =
    let MapKeyToInputCommand
            (key : ConsoleKey)
            : InputCommand option =
        match key with
        | ConsoleKey.LeftArrow -> Left |> Some
        | ConsoleKey.RightArrow -> Right |> Some
        | ConsoleKey.UpArrow -> Up |> Some
        | ConsoleKey.DownArrow -> Down |> Some

        | ConsoleKey.Z -> Red |> Some
        | ConsoleKey.X -> Green |> Some
        | ConsoleKey.C -> Blue |> Some
        | ConsoleKey.E -> Yellow |> Some

        | ConsoleKey.Escape -> Back |> Some
        | ConsoleKey.F2 -> Start |> Some

        | ConsoleKey.OemComma ->  Previous |> Some
        | ConsoleKey.OemPeriod -> Next |> Some

        | _ -> None

    let mutable internal commandQueue : InputCommand list = []

    let Get() =
        match commandQueue with
        | [] ->
            None
        | head :: tail ->
            commandQueue <- tail
            head
            |> Some


