﻿namespace Splorr.Crucible.Host

open Splorr.Common
open Microsoft.Xna.Framework
open Splorr.Common.Runner
open Microsoft.Xna.Framework.Graphics
open Microsoft.Xna.Framework.Input
open System.IO
open System
open Splorr.Crucible.Runner

type HostGame(context : CommonContext) as this =
    inherit Game()

    do
        this.Content.RootDirectory <- "Content"

    let graphics = new GraphicsDeviceManager(this)
    let context = context
    let mutable spriteBatch : SpriteBatch = null
    let mutable oldKeyboardState: KeyboardState = Keyboard.GetState()
    let mutable texture : Texture2D = null
    let mutable blankTexture : Texture2D = null
    let mutable runnerState : RunnerState = Title
    let hueTable : Map<Hue, Color> =
        [
            Black, Color(0,0,0)
            DarkGray, Color(87,87,87)
            Hue.Red, Color(173,35,35)
            Hue.Blue, Color(42,75,215)
            Hue.Green, Color(29,105,20)
            Brown, Color(129,74,25)
            Purple, Color(129,38,192)
            LightGray, Color(160,160,160)
            LightGreen, Color(129,197,122)
            LightBlue, Color(157,175,255)
            Cyan, Color(41,208,208)
            Orange, Color(255,146,51)
            Hue.Yellow, Color(255,238,51)
            Tan, Color(233,222,187)
            Pink, Color(255,205,243)
            White, Color(255,255,255)
        ]
        |> Map.ofList

    

    override this.Initialize() =
        this.Window.Title <- Host.GetTitle context
        Host.GetGridSize context
        |> Output.SetSize context 
        let width, height = Host.GetPreferredBackBufferSize context
        graphics.PreferredBackBufferWidth <- width
        graphics.PreferredBackBufferHeight <- height
        //TODO : figure out how to turn on the mag/min filters
        
        graphics.ApplyChanges()
        spriteBatch <- new SpriteBatch(this.GraphicsDevice)
        texture <- Texture2D.FromStream(this.GraphicsDevice, new FileStream("Content/romfont8x8.png", FileMode.Open))
        blankTexture <- Texture2D.FromStream(this.GraphicsDevice, new FileStream("Content/blank.png", FileMode.Open))
        base.Initialize()

    override this.LoadContent() =
        //Intentionally blank
        ()

    override this.Update (delta:GameTime)  =
        let state = Keyboard.GetState()
        Host.GetKeyCommands context
        |> Map.iter
            (fun key command ->
                if state.IsKeyDown key && (not <| oldKeyboardState.IsKeyDown key) then
                    InputImplementation.commandQueue <- List.append InputImplementation.commandQueue [command])
        oldKeyboardState <- state
        match Runner.Update context runnerState with
        | Some state ->
            runnerState <- state
        | _ ->
            this.Exit()

    override this.Draw (delta:GameTime) =
        Color.Black
        |>  this.GraphicsDevice.Clear
        spriteBatch.Begin()
        let gridSize = Host.GetGridSize context

        [0..((gridSize |> fst) - 1)]
        |> List.iter
            (fun column ->
                [0..((gridSize |> snd) - 1)]
                |> List.iter
                    (fun row ->
                        let boardCell = OutputImplementation.board.GetCell(column,row) |> Option.get
                        let cellColumn = (boardCell.glyph % 16uy) |> int
                        let cellRow = (boardCell.glyph / 16uy) |> int
                        let destination = Rectangle(column * 16, row * 16, 16, 16)
                        spriteBatch.Draw(blankTexture, destination, hueTable.[boardCell.background])
                        spriteBatch.Draw(texture, destination, Nullable<Rectangle>(Rectangle(cellColumn * 16, cellRow* 16, 16, 16)), hueTable.[boardCell.foreground])))
        spriteBatch.End()


