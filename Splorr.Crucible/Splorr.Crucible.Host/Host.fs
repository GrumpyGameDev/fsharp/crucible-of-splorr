﻿namespace Splorr.Crucible.Host

open Splorr.Common
open System
open Microsoft.Xna.Framework.Input
open Splorr.Crucible.Runner

module Host =
    type PreferredBackBufferSizeSource = unit -> int * int
    type GetPreferredBackBufferSizeContext =
        abstract member GetPreferredBackBufferSize : PreferredBackBufferSizeSource ref
    let GetPreferredBackBufferSize 
            (context : CommonContext)
            : int * int =
        (context :?> GetPreferredBackBufferSizeContext).GetPreferredBackBufferSize.Value()

    type TitleSource = unit -> string
    type GetTitleContext =
        abstract member GetTitle : TitleSource ref
    let GetTitle
            (context : CommonContext)
            : string =
        (context :?> GetTitleContext).GetTitle.Value()

    type GridSizeSource = unit -> int * int
    type GetGridSizeContext =
        abstract member GetGridSize : GridSizeSource ref
    let GetGridSize
            (context : CommonContext)
            : int * int =
        (context :?> GetGridSizeContext).GetGridSize.Value()

    type KeyCommandsSource = unit -> Map<Keys, InputCommand>
    type GetKeyCommandsContext =
        abstract member GetKeyCommands : KeyCommandsSource ref
    let GetKeyCommands 
            (context : CommonContext)
            : Map<Keys, InputCommand> =
        (context :?> GetKeyCommandsContext).GetKeyCommands.Value()