﻿namespace Splorr.Common

open System

type Board<'TCell>(columns : int, rows: int, defaultCell:'TCell) =
    let columns = max columns 1
    let rows = max rows 1
    let defaultCell : 'TCell = defaultCell
    let mutable cells : Map<int * int, 'TCell> = Map.empty
    member this.Columns : int = columns 
    member this.Rows : int = rows
    member this.GetCell(column:int, row:int) : 'TCell option =
        if column>=0 && column <columns && row>=0 && row<rows then
            cells
            |> Map.tryFind (column, row)
            |> Option.defaultValue defaultCell
            |> Some
        else
            None
    member this.SetCell(column:int, row:int, cell: 'TCell) : unit =
        if column>=0 && column<columns && row>=0 && row<rows then
            cells <- cells |> Map.add (column, row) cell
        


