﻿namespace Splorr.Common.Runner

type RunnerContext<'TCommand> =
    inherit Input.PollForKeyContext<'TCommand>
    inherit Output.SetSizeContext
    inherit Output.SetPositionContext
    inherit Output.WriteTextContext
    inherit Output.ClearContext

