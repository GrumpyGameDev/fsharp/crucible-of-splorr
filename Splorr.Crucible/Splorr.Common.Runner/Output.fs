﻿namespace Splorr.Common.Runner

open System
open Splorr.Common

module Output =
    type StringSink = string -> unit

    type IntTupleSink = int * int -> unit
    type SetSizeContext =
        abstract member PutSize : IntTupleSink ref
    let SetSize
            (context: CommonContext)
            (size : int * int) 
            : unit =
        (context :?> SetSizeContext).PutSize.Value size

    type SetPositionContext =
        abstract member PutPosition : IntTupleSink ref
    let SetPosition
            (context : CommonContext)
            (position : int * int)
            : unit =
        (context :?> SetPositionContext).PutPosition.Value position

    type WriteTextContext =
        abstract member PutText : StringSink ref
    let WriteText
            (context : CommonContext)
            (text : string)
            : unit =
        (context :?> WriteTextContext).PutText.Value text

    type HueSink = Hue -> unit
    type SetForegroundColorContext =
        abstract member PutForegroundHue : HueSink ref
    let SetForegroundColor
            (context : CommonContext)
            (color : Hue)
            : unit =
        (context :?> SetForegroundColorContext).PutForegroundHue.Value color

    
    type SetBackgroundColorContext =
        abstract member PutBackgroundHue : HueSink ref
    let SetBackgroundColor
            (context : CommonContext)
            (color : Hue)
            : unit =
        (context :?> SetBackgroundColorContext).PutBackgroundHue.Value color

    type Sink = unit -> unit
    type ClearContext =
        abstract member PutClear: Sink ref
    let Clear
            (context : CommonContext)
            : unit =
        (context :?> ClearContext).PutClear.Value()

    let ClearColor
            (context : CommonContext)
            (color : Hue)
            : unit =
        SetBackgroundColor context color
        Clear context

    

