﻿namespace Splorr.Common.Runner

open System
open Splorr.Common

type MenuItem<'TValue when 'TValue:equality> =
    {
        position : int * int
        value : 'TValue
        text : string
    }
    with
        static member Create(position:int * int, value: 'TValue, text :string) : MenuItem<'TValue> =
            {
                position = position
                value = value
                text = text
            }
        static member Render
                (context : CommonContext)
                (menuHues : Map<bool, Hue>)
                (menuitems : MenuItem<'TValue> list)
                (state : 'TValue)
                : unit =
            let trueColor = 
                menuHues 
                |> Map.tryFind true 
                |> Option.defaultValue Yellow
            let falseColor = 
                menuHues 
                |> Map.tryFind false 
                |> Option.defaultValue Black
            menuitems
            |> List.iter
                (fun menuitem ->
                    let foregroundColor, backgroundColor =
                        if menuitem.value=state then
                            falseColor, trueColor
                        else
                            trueColor, falseColor
                    Output.SetPosition context (menuitem.position |> fst, menuitem.position |> snd)
                    Output.SetForegroundColor context foregroundColor
                    Output.SetBackgroundColor context backgroundColor
                    Output.WriteText context menuitem.text)

