﻿namespace Splorr.Common.Runner

open System
open Splorr.Common

type StaticText =
    {
        position : int * int
        foregroundColor: Hue
        backgroundColor: Hue option
        text : string
    }
    with
        static member Create 
                (position : int * int, 
                text: string, 
                foregroundColor : Hue, 
                backgroundColor: Hue option) : StaticText =
            {
                position = position
                text = text
                foregroundColor = foregroundColor
                backgroundColor = backgroundColor
            }
        static member Render
                (context : CommonContext)
                (caption : StaticText)
                : unit =
            Output.SetForegroundColor context caption.foregroundColor
            caption.backgroundColor
            |> Option.iter (Output.SetBackgroundColor context)
            Output.SetPosition context caption.position
            Output.WriteText context caption.text
