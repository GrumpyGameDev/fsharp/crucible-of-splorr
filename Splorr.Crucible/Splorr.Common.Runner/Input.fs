﻿namespace Splorr.Common.Runner

open System
open Splorr.Common

module Input =
    type CommandOptionSource<'TCommand> = unit -> 'TCommand option
    type PollForKeyContext<'TCommand> =
        abstract member GetCommand : CommandOptionSource<'TCommand> ref
    let PollForKey<'TCommand>
            (context : CommonContext)
            : 'TCommand option =
        (context :?> PollForKeyContext<'TCommand>).GetCommand.Value()
