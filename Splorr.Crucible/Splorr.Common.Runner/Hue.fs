﻿namespace Splorr.Common.Runner

//from http://alumni.media.mit.edu/~wad/color/numbers.html
type Hue =
    | Black //0,0,0
    | DarkGray //87,87,87
    | Red //173,35,35
    | Blue //42,75,215
    | Green //29,105,20
    | Brown //129,74,25
    | Purple //129,38,192
    | LightGray //160,160,160
    | LightGreen //129,197,122
    | LightBlue //157,175,255
    | Cyan //41,208,208
    | Orange //255,146,51
    | Yellow //255,238,51
    | Tan //233,222,187
    | Pink //255,205,243
    | White //255,255,255

