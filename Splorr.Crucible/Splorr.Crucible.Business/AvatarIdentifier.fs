﻿namespace Splorr.Crucible.Business

open Splorr.Common
open System
open Splorr.Crucible.Model

module AvatarIdentifier =
    type GenerateSource = unit -> AvatarIdentifier
    type GenerateContext =
        abstract member GenerateAvatarIdentifier : GenerateSource ref
    let Generate
            (context : CommonContext)
            : AvatarIdentifier =
        (context :?> GenerateContext).GenerateAvatarIdentifier.Value()

