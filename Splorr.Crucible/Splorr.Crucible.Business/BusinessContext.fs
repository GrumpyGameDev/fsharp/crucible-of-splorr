﻿namespace Splorr.Crucible.Business

open Splorr.Common

type BusinessContext =
    inherit CommonContext
    inherit AvatarIdentifier.GenerateContext

