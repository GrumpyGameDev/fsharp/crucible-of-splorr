﻿namespace Splorr.Crucible.Persistence

module Say =
    let hello name =
        printfn "Hello %s" name
