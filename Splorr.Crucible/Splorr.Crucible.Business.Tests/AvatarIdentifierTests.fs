﻿module Splorr.Crucible.Business.AvatarIdentifierTests

open NUnit.Framework
open System
open Splorr.Tests.Common

[<Test>]
let ``Generate.It generates a new avatar identifier.``() =
    let generated = Guid.NewGuid()
    let context = Contexts.TestContext()
    let called : bool ref = ref false
    (context :> AvatarIdentifier.GenerateContext).GenerateAvatarIdentifier := Spies.Source (called, generated)
    let actual = AvatarIdentifier.Generate context
    Assert.AreEqual(generated, actual)
    Assert.IsTrue(called.Value)
    

