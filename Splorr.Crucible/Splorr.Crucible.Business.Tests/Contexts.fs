﻿module Splorr.Crucible.Business.Contexts

open System
open Splorr.Common
open Splorr.Tests.Common

type TestContext
        () =
    interface CommonContext
    interface BusinessContext
    interface AvatarIdentifier.GenerateContext with
        member val GenerateAvatarIdentifier = ref (Fakes.Source ("AvatarIdentifier.GenerateContext", Guid.Empty))

