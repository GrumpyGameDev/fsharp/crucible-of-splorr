﻿module Splorr.Tests.Common.Mocks

open NUnit.Framework

let Expect (expectSunk:'TSunk) (actualSunk:'TSunk) : unit =
    Assert.AreEqual(expectSunk, actualSunk)


