﻿module Splorr.Tests.Common.Spies

let Source<'TFilter,'TResult>(flag:bool ref,result:'TResult) (_:'TFilter) : 'TResult =
    flag := true
    result
let Sink(flag:bool ref)(sunk:'TSunk) : unit = Source<'TSunk, unit> (flag, ()) sunk

