﻿namespace Splorr.Crucible.Runner

open System
open Splorr.Common
open Splorr.Common.Runner
open Splorr.Crucible.Business

module MainMenu =
    let private menuHues =
        Map.empty
        |> Map.add true White
        |> Map.add false Blue

    let private menuitems : MenuItem<MainMenuState> list =
        [
            MenuItem.Create((35, 15), MainMenuState.Play,         "    Play    ")
            MenuItem.Create((35, 16), MainMenuState.Instructions, "Instructions")
            MenuItem.Create((35, 17), Quit,                       "    Quit    ")
        ]

    let private captions : StaticText list =
        [
            StaticText.Create((35,13), " Main  Menu ", White, None)
            StaticText.Create((31,28), "\u0019\u0018 - Next/Previous", DarkGray, None)
            StaticText.Create((30,29), "<X> - Perform Action", DarkGray, None)
            
        ]

    let private UpdateDisplay
            (context : CommonContext)
            (state: MainMenuState)
            : unit =
        Output.ClearColor context Black
        captions
        |> List.iter (StaticText.Render context)
        MenuItem.Render context menuHues menuitems state
        
    let private HandleInput
            (context : CommonContext)
            (state : MainMenuState)
            : RunnerState option =
        match Input.PollForKey<InputCommand> context with
        | Some Up ->
            state.Previous
            |> RunnerState.MainMenu
            |> Some
        | Some Down ->
            state.Next
            |> RunnerState.MainMenu
            |> Some
        | Some InputCommand.Green ->
            match state with
            | Quit ->
                RunnerState.Confirm ("Are you sure you want to quit?", ConfirmState.No, None, state |> RunnerState.MainMenu |> Some)
                |> Some
            | MainMenuState.Instructions ->
                RunnerState.Instructions (1, state |> RunnerState.MainMenu)
                |> Some
            | MainMenuState.Play ->
                AvatarIdentifier.Generate context
                |> RunnerState.Play
                |> Some
        | _ ->
            state
            |> RunnerState.MainMenu
            |> Some

    let Update
            (context : CommonContext)
            (state : MainMenuState)
            : RunnerState option =
        UpdateDisplay context state
        HandleInput context state



