﻿namespace Splorr.Crucible.Runner

open System
open Splorr.Common
open Splorr.Common.Runner

module Runner =
    let Update
            (context:CommonContext) 
            (state : RunnerState)
            : RunnerState option =
        match state with
        | RunnerState.Title ->
            Title.Update context
        | RunnerState.MainMenu state ->
            MainMenu.Update context state
        | RunnerState.Confirm (prompt, state, confirmState, cancelState) ->
            Confirm.Update context (prompt, state, confirmState, cancelState)
        | RunnerState.Instructions (page, previousState) ->
            Instructions.Update context (page, previousState)
        | RunnerState.Play avatarIdentifier ->
            Play.Update context avatarIdentifier
        | _ ->
            raise (NotImplementedException "BAD RUNNER STATE")
        
