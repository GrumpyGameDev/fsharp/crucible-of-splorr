﻿namespace Splorr.Crucible.Runner

open System
open Splorr.Common
open Splorr.Common.Runner

module Instructions =
    let private captions : StaticText list =
        [
            StaticText.Create((18,28), "\u001b \u001a - Next/Previous Page", DarkGray, None)
            StaticText.Create((23,29), "<Z> - Go  Back", DarkGray, None)
        ]

    let pageOneCaptions : StaticText list =
        [
            StaticText.Create((17,0), "Instructions (Page 1 of 1)", Cyan, None)
            StaticText.Create((13,15), "TODO:  Put some instructions here!", LightGray, None)
        ]

    let private pageCaptions : Map<int, StaticText list> =
        Map.empty
        |> Map.add 1 pageOneCaptions
    
    let UpdateDisplay
            (context : CommonContext)
            (page : int) =
        Output.ClearColor context Black
        pageCaptions
        |> Map.tryFind page
        |> Option.iter 
            (List.iter (StaticText.Render context))
        captions
        |> List.iter (StaticText.Render context)

    let private NextPage (page : int) = page
    let private PreviousPage (page : int) = page

    let HandleInput
            (context : CommonContext)
            (page : int, previousState : RunnerState) 
            : RunnerState option =
        match Input.PollForKey<InputCommand> context with
        | Some Back
        | Some InputCommand.Red ->
            previousState
            |> Some
        | Some Left ->
            (page
            |> NextPage, previousState)
            |> RunnerState.Instructions
            |> Some
        | Some Right ->
            (page
            |> PreviousPage, previousState)
            |> RunnerState.Instructions
            |> Some
        | _ ->
            (page, previousState)
            |> RunnerState.Instructions
            |> Some

    let Update
            (context : CommonContext)
            (page : int, previousState : RunnerState)
            : RunnerState option =
        UpdateDisplay context page
        HandleInput context (page, previousState)

