﻿namespace Splorr.Crucible.Runner

open System
open Splorr.Common
open Splorr.Common.Runner

module Title =
    let Update
            (context : CommonContext) 
            : RunnerState option =
        Output.ClearColor context Black
        Output.SetPosition context (30, 11)
        Output.SetForegroundColor context LightBlue
        Output.WriteText context "Crucible of SPLORR!!"
        Output.SetPosition context (33, 22)
        Output.SetForegroundColor context Cyan
        Output.WriteText context "a production of"
        Output.SetPosition context (31, 33)
        Output.SetForegroundColor context Purple
        Output.WriteText context "The Grumpy GameDev"
        Output.SetPosition context (34, 44)
        Output.SetForegroundColor context DarkGray
        Output.WriteText context "<F2> to Start"

        match Input.PollForKey<InputCommand> context with
        | Some Start ->
            MainMenuState.Play
            |> RunnerState.MainMenu
            |> Some
        | _ ->
            RunnerState.Title
            |> Some

