﻿namespace Splorr.Crucible.Runner

open Splorr.Crucible.Model

type MainMenuState =
    | Play
    | Instructions
    | Quit
    with
        member x.Next =
            match x with
            | Play -> Instructions
            | Instructions -> Quit
            | Quit -> Play
        member x.Previous =
            match x with
            | Play -> Quit
            | Instructions -> Play
            | Quit -> Instructions

type ConfirmState =
    | No
    | Yes
    with 
        member x.Next =
            match x with 
            | No -> Yes
            | Yes -> No
        member x.Previous = x.Next

type RunnerState =
    | Title
    | Instructions of int * RunnerState
    | MainMenu of MainMenuState
    | Play of AvatarIdentifier
    | Confirm of string * ConfirmState * RunnerState option * RunnerState option


