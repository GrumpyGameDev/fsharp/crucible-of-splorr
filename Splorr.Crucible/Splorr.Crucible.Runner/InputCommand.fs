﻿namespace Splorr.Crucible.Runner

type InputCommand =
    | Up
    | Down
    | Left
    | Right

    | Green
    | Blue
    | Yellow
    | Red

    | Start
    | Back

    | Next
    | Previous



