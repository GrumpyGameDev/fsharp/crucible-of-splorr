﻿namespace Splorr.Crucible.Runner

open Splorr.Common
open System
open Splorr.Common.Runner

module Confirm =
    let private menuitems :MenuItem<ConfirmState> list=
        [
            MenuItem.Create((0, 2), No,  "No ")
            MenuItem.Create((0, 3), Yes, "Yes")
        ]

    let private captions(prompt:string) : StaticText list =
        [
            StaticText.Create((0,0), prompt, White, None)
            StaticText.Create((21,28), "\u0019\u0018 - Next/Previous", DarkGray, None)
            StaticText.Create((20,29), "<X> - Perform Action", DarkGray, None)
        ]

    let private UpdateDisplay
            (context : CommonContext)
            (prompt : string)
            (state : ConfirmState)
            : unit =
        Output.ClearColor context Black
        captions(prompt)
        |> List.iter (StaticText.Render context)

        MenuItem.Render context Map.empty menuitems state

    let private HandleInput
            (context : CommonContext)
            (prompt:string, state: ConfirmState, confirmState : RunnerState option, cancelState: RunnerState option)
            : RunnerState option =
        match Input.PollForKey<InputCommand> context with
        | Some Up ->
            (prompt, state.Previous, confirmState, cancelState)
            |> Confirm
            |> Some
        | Some Down ->
            (prompt, state.Next, confirmState, cancelState)
            |> Confirm
            |> Some
        | Some InputCommand.Green ->
            match state with
            | Yes -> confirmState
            | No -> cancelState
        | _ ->
            (prompt, state, confirmState, cancelState)
            |> Confirm
            |> Some

    let Update
            (context : CommonContext)
            (prompt:string, state: ConfirmState, confirmState : RunnerState option, cancelState: RunnerState option)
            : RunnerState option =
        UpdateDisplay
            context
            prompt
            state
        HandleInput
            context
            (prompt, state, confirmState, cancelState)


