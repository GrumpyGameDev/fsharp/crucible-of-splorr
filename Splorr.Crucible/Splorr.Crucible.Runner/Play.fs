﻿namespace Splorr.Crucible.Runner

open Splorr.Common
open System
open Splorr.Common.Runner
open Splorr.Crucible.Business
open Splorr.Crucible.Model

module Play =
    let private DrawCell
            (context : CommonContext)
            (screenLocation : Location)
            (gameLocation : Location)
            (avatarIdentifier : AvatarIdentifier)
            : unit =
        ()
    
    let private UpdateDisplay
            (context : CommonContext)
            (avatarIdentifier : AvatarIdentifier)
            : unit =
        Output.ClearColor context Black
        [0..39]
        |> List.iter
            (fun column ->
                [0..29]
                |> List.iter 
                    (fun row->
                        DrawCell context (column, row) (column - 20, row - 15) avatarIdentifier))


    let private HandleInput
            (context : CommonContext)
            (avatarIdentifier : AvatarIdentifier)
            : RunnerState option =
        match Input.PollForKey<InputCommand> context with
        | Some Back ->
            RunnerState.Confirm ("Are you sure you want to abandon the game?", ConfirmState.No, RunnerState.MainMenu MainMenuState.Play |> Some, RunnerState.Play avatarIdentifier |> Some)
            |> Some
        | _ ->
            RunnerState.Play avatarIdentifier
            |> Some

    let Update
            (context : CommonContext)
            (avatarIdentifier : AvatarIdentifier)
            : RunnerState option =
        UpdateDisplay context avatarIdentifier
        HandleInput context avatarIdentifier

