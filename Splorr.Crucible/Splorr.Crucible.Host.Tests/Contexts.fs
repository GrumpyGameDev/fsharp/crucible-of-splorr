﻿module Splorr.Crucible.Host.Contexts

open Splorr.Common
open Splorr.Common.Runner
open Splorr.Crucible.Runner
open Splorr.Crucible.Business
open Splorr.Tests.Common
open System
    
type TestContext() =
    interface CommonContext
    interface RunnerContext<InputCommand>
    interface Input.PollForKeyContext<InputCommand> with
        member this.GetCommand = raise (System.NotImplementedException())
    interface Output.ClearContext with
        member this.PutClear = raise (System.NotImplementedException())
    interface Output.SetBackgroundColorContext with
        member this.PutBackgroundHue = raise (System.NotImplementedException())
    interface Output.SetForegroundColorContext with
        member this.PutForegroundHue = raise (System.NotImplementedException())
    interface Output.SetSizeContext with
        member this.PutSize = raise (System.NotImplementedException())
    interface Output.SetPositionContext with
        member this.PutPosition = raise (System.NotImplementedException())
    interface Output.WriteTextContext with
        member this.PutText = raise (System.NotImplementedException())
    interface BusinessContext
    interface HostContext
    interface Host.GetPreferredBackBufferSizeContext with
        member val GetPreferredBackBufferSize = ref (Fakes.Source ("Host.GetPreferredBackBufferSizeContext",(0,0)))
    interface Host.GetTitleContext with
        member val GetTitle = ref (Fakes.Source ("Host.GetTitleContext",""))
    interface Host.GetGridSizeContext with
        member val GetGridSize = ref (Fakes.Source ("Host.GetGridSizeContext",(0,0)))
    interface Host.GetKeyCommandsContext with
        member val GetKeyCommands = ref (Fakes.Source ("Host.GetKeyCommandsContext",Map.empty))
    interface AvatarIdentifier.GenerateContext with
        member val GenerateAvatarIdentifier = ref (Fakes.Source ("AvatarIdentifier.GenerateContext", Guid.Empty))

