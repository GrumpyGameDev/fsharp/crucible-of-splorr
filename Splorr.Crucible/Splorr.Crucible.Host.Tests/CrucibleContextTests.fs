module Splorr.Crucible.Host.CrucibleContextTests

open NUnit.Framework
open Splorr.Common
open Splorr.Common.Runner
open System
open Splorr.Crucible.Runner

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``CrucibleContext.It has all of the context interfaces that the game needs.`` () =
    let subject = CrucibleContext() :> CommonContext
    Assert.DoesNotThrow(fun () -> subject :?> Splorr.Common.Runner.RunnerContext<InputCommand> |> ignore)
    Assert.DoesNotThrow(fun () -> subject :?> Splorr.Crucible.Business.BusinessContext |> ignore)

    Assert.Pass("If we made it this far, we are doing well!")

