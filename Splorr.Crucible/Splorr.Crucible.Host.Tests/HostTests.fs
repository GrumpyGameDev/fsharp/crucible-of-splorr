﻿module Splorr.Crucible.Host.HostTests

open NUnit.Framework
open Splorr.Tests.Common


[<Test>]
let ``GetPreferredBackBufferSize.It retrieves the preferred back buffer size.`` () =
    let context =
        Contexts.TestContext()
    (context :> Host.GetPreferredBackBufferSizeContext).GetPreferredBackBufferSize := Stubs.Source (640, 480)
    let actual = Host.GetPreferredBackBufferSize context
    Assert.AreEqual((640, 480), actual)

[<Test>]
let ``GetTitle.It retrieves the title for the window.`` () =
    let context =
        Contexts.TestContext()
    (context :> Host.GetTitleContext).GetTitle := Stubs.Source "?"
    let actual = Host.GetTitle context
    Assert.AreEqual("?", actual)

[<Test>]
let ``GetGridSize.It retrieves the grid size for the game.`` () =
    let context =
        Contexts.TestContext()
    (context :> Host.GetGridSizeContext).GetGridSize := Stubs.Source (1,2)
    let actual = Host.GetGridSize context
    Assert.AreEqual((1,2), actual)
    

[<Test>]
let ``GetKeyCommands.It retrieves the key commands for the game.`` () =
    let context =
        Contexts.TestContext()
    (context :> Host.GetKeyCommandsContext).GetKeyCommands := Stubs.Source Map.empty
    let actual = Host.GetKeyCommands context
    Assert.AreEqual(Map.empty, actual)

