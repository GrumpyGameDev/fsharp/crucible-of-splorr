﻿module Splorr.Crucible.Host.InputImplementationMapKeyToInputCommandTests

open System
open NUnit.Framework
open Splorr.Crucible.Runner

[<Test>]
let ``MapKeyToInputCommand.It maps a console key to the equivalent input command.`` () =
    [
        ConsoleKey.LeftArrow, Some Left
        ConsoleKey.RightArrow, Some Right
        ConsoleKey.DownArrow, Some Down
        ConsoleKey.UpArrow, Some Up
        ConsoleKey.Z, Some Red
        ConsoleKey.X, Some Green
        ConsoleKey.C, Some Blue
        ConsoleKey.E, Some Yellow
        ConsoleKey.F2, Some Start
        ConsoleKey.Escape, Some Back
        ConsoleKey.OemComma, Some Previous
        ConsoleKey.OemPeriod, Some Next
    ]
    |> List.iter
        (fun (key, command) ->
            Assert.AreEqual(command, key |> InputImplementation.MapKeyToInputCommand))
    

