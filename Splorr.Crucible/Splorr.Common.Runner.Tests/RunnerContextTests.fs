﻿module Splorr.Common.Runner.RunnerContextTests

open NUnit.Framework
open Splorr.Tests.Common

let ``Constructor.It can downcast to all of the intervaces in common runner.``()=
    let context = 
        Contexts.TestContext()
    Assert.DoesNotThrow(fun () -> context :> Input.PollForKeyContext<System.ConsoleKey> |> ignore)
    Assert.DoesNotThrow(fun () -> context :> Output.SetSizeContext |> ignore)
    Assert.DoesNotThrow(fun () -> context :> Output.WriteTextContext |> ignore)
    Assert.DoesNotThrow(fun () -> context :> Output.ClearContext |> ignore)
    Assert.DoesNotThrow(fun () -> context :> Output.SetBackgroundColorContext |> ignore)
    Assert.DoesNotThrow(fun () -> context :> Output.SetForegroundColorContext |> ignore)
    Assert.DoesNotThrow(fun () -> context :> Output.SetPositionContext |> ignore)

