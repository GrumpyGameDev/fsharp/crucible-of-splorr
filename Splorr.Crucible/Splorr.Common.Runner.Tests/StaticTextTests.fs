﻿module Splorr.Common.Runner.StaticTextTests

open NUnit.Framework
open System
open Splorr.Tests.Common

[<Test>]
let ``Create.It creates a static text with the given properties.`` () =
    let actual =
        StaticText.Create((1,2), "text", White, None)
    Assert.AreEqual((1,2), actual.position)
    Assert.AreEqual("text", actual.text)
    Assert.AreEqual(White, actual.foregroundColor)
    Assert.AreEqual(None, actual.backgroundColor)

[<Test>]
let ``Render.It renders the static text without setting a background color when the background color is omitted.`` () =
    let caption =
        StaticText.Create((1,2), "text", White, None)
    let context = 
        Contexts.TestContext()
    (context :> Output.SetForegroundColorContext).PutForegroundHue := Mocks.Expect White
    (context :> Output.SetPositionContext).PutPosition := Mocks.Expect (1,2)
    (context :> Output.WriteTextContext).PutText := Mocks.Expect "text"
    StaticText.Render
        context
        caption

[<Test>]
let ``Render.It renders the static text with setting a background color when the background color is present.`` () =
    let caption =
        StaticText.Create((1,2), "text", White, Some Black)
    let context = 
        Contexts.TestContext()
    (context :> Output.SetForegroundColorContext).PutForegroundHue := Mocks.Expect White
    (context :> Output.SetBackgroundColorContext).PutBackgroundHue := Mocks.Expect Black
    (context :> Output.SetPositionContext).PutPosition := Mocks.Expect (1,2)
    (context :> Output.WriteTextContext).PutText := Mocks.Expect "text"
    StaticText.Render
        context
        caption
    
    