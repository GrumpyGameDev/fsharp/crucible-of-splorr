﻿module Splorr.Common.Runner.OutputSetBackgroundColorTests

open NUnit.Framework
open Splorr.Common
open System
open Splorr.Tests.Common


[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``SetBackgroundColor.It sets the foreground color.`` () =
    let mutable called = false
    let sink(c) : unit =
        called <- true
        Assert.AreEqual(Blue, c)
    let context = Contexts.TestContext()
    (context :> Output.SetBackgroundColorContext).PutBackgroundHue := sink
    Output.SetBackgroundColor context Blue
    Assert.IsTrue(called)
