﻿module Splorr.Common.Runner.OutputSetForegroundColorTests

open NUnit.Framework
open Splorr.Common
open System
open Splorr.Tests.Common

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``SetForegroundColor.It sets the foreground color.`` () =
    let mutable called = false
    let sink(c) : unit =
        called <- true
        Assert.AreEqual(Blue, c)
    let context = Contexts.TestContext()
    (context :> Output.SetForegroundColorContext).PutForegroundHue := sink
    Output.SetForegroundColor context Blue
    Assert.IsTrue(called)
