﻿module Splorr.Common.Runner.OutputSetPositionTests

open NUnit.Framework
open Splorr.Common
open System

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``SetPosition.It sets the position.`` () =
    let called = ref false
    let sink (w,h) =
        Assert.AreEqual(20,w)
        Assert.AreEqual(15,h)
        called := true
    let context = Contexts.TestContext()
    (context :> Output.SetPositionContext).PutPosition := sink
    Output.SetPosition context (20,15)
    Assert.IsTrue(called.Value)
