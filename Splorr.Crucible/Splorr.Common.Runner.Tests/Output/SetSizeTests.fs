﻿module Splorr.Common.Runner.OutputSetSizeTests

open NUnit.Framework
open Splorr.Common
open System

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``SetSize.It sets the size.`` () =
    let mutable called = false
    let sink (w,h) =
        Assert.AreEqual(40,w)
        Assert.AreEqual(30,h)
        called <- true
    let context = Contexts.TestContext()
    (context :> Output.SetSizeContext).PutSize := sink
    Output.SetSize context (40,30)
    Assert.IsTrue(called)
