﻿module Splorr.Common.Runner.OutputClearTests

open NUnit.Framework
open Splorr.Common
open System
open Splorr.Tests.Common

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``Clear.It clears the console.`` () =
    let mutable called = false
    let clearSink() =
        called <- true
    let context = Contexts.TestContext()
    (context :> Output.ClearContext).PutClear := clearSink
    Output.Clear context
    Assert.IsTrue(called)

[<Test>]
let ``ClearColor.It clears the console a particular color.`` () =
    let mutable calledClearSink = false
    let mutable calledBackgroundColorSink = false
    let clearSink() =
        calledClearSink <- true
    let backgroundColorSink(_) =
        calledBackgroundColorSink <- true
    let context = Contexts.TestContext()
    (context :> Output.SetBackgroundColorContext).PutBackgroundHue := backgroundColorSink
    (context :> Output.ClearContext).PutClear := clearSink
    Output.ClearColor context Black
    Assert.IsTrue(calledClearSink)
    Assert.IsTrue(calledBackgroundColorSink)
