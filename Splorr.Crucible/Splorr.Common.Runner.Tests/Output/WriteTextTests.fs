﻿module Splorr.Common.Runner.OutputWriteTextTests

open NUnit.Framework
open Splorr.Common
open System

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``WriteText.It writes text.`` () =
    let mutable called = false
    let sink(t) : unit =
        called <- true
        Assert.AreEqual("This is text", t)
    let context = Contexts.TestContext()
    (context :> Output.WriteTextContext).PutText := sink
    Output.WriteText context "This is text"
    Assert.IsTrue(called)

