﻿module Splorr.Common.Runner.Contexts

open Splorr.Common
open Splorr.Tests.Common
open NUnit.Framework

type TestContext() =
    interface CommonContext
    interface RunnerContext<System.ConsoleKey>
    interface Input.PollForKeyContext<System.ConsoleKey> with
        member val GetCommand = ref (Fakes.Source ("PollForKeyContext", None))
    interface Output.SetSizeContext with
        member val PutSize = ref (Fakes.Sink "SetSizeContext")
    interface Output.SetPositionContext with
        member val PutPosition = ref (Fakes.Sink "SetPositionContext")
    interface Output.SetForegroundColorContext with
        member val PutForegroundHue = ref (Fakes.Sink "SetForegroundColorContext")
    interface Output.SetBackgroundColorContext with
        member val PutBackgroundHue = ref (Fakes.Sink "SetBackgroundColorContext")
    interface Output.WriteTextContext with
        member val PutText = ref (Fakes.Sink "WriteTextContext")
    interface Output.ClearContext with
        member val PutClear = ref (Fakes.Sink "ClearContext")


