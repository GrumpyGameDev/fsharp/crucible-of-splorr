﻿module Splorr.Common.Runner.InputTests

open NUnit.Framework
open Splorr.Common
open System
open Splorr.Tests.Common

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``PollForKey.It returns None when there is no character waiting.`` () =
    let called = ref false
    let context = Contexts.TestContext()
    (context :> Input.PollForKeyContext<System.ConsoleKey>).GetCommand := Spies.Source (called, None)
    let actual = Input.PollForKey<ConsoleKey> context
    Assert.AreEqual(None, actual)
    Assert.IsTrue(called.Value)

[<Test>]
let ``PollForKey.It returns the key when there is a character waiting.`` () =
    let called = ref false
    let context = Contexts.TestContext()
    (context :> Input.PollForKeyContext<System.ConsoleKey>).GetCommand := Spies.Source (called, ConsoleKey.Spacebar |> Some)
    let actual = Input.PollForKey<ConsoleKey> context
    Assert.AreEqual(Some ConsoleKey.Spacebar, actual)
    Assert.IsTrue(called.Value)
