﻿module Splorr.Common.Runner.MenuItemTests

open NUnit.Framework
open Splorr.Common
open System
open Splorr.Tests.Common

[<SetUp>]
let Setup () =
    ()

[<Test>]
let ``Create.It creates a menu item with the given properties.`` () =
    let actual =
        MenuItem.Create((1,2), "value", "text")
    Assert.AreEqual((1,2), actual.position)
    Assert.AreEqual("value", actual.value)
    Assert.AreEqual("text", actual.text)

[<Test>]
let ``Render.It renders menu items to the output with the selected item with colors inverted.`` () =
    let context = 
        Contexts.TestContext()
    (context :> Output.SetPositionContext).PutPosition := Mocks.Expect(1,2)
    (context :> Output.SetBackgroundColorContext).PutBackgroundHue := Mocks.Expect Yellow
    (context :> Output.SetForegroundColorContext).PutForegroundHue := Mocks.Expect Black
    (context :> Output.WriteTextContext).PutText := Mocks.Expect "text"
    MenuItem.Render
        context
        Map.empty
        [
            MenuItem.Create((1,2), "value", "text")
        ]
        "value"

[<Test>]
let ``Render.It renders menu items to the output with the non-selected item with usual colors.`` () =
    let context = 
        Contexts.TestContext()
    (context :> Output.SetPositionContext).PutPosition := Mocks.Expect(1,2)
    (context :> Output.SetBackgroundColorContext).PutBackgroundHue := Mocks.Expect Black
    (context :> Output.SetForegroundColorContext).PutForegroundHue := Mocks.Expect Yellow
    (context :> Output.WriteTextContext).PutText := Mocks.Expect "text"
    MenuItem.Render
        context
        Map.empty
        [
            MenuItem.Create((1,2), "value", "text")
        ]
        ""


[<Test>]
let ``Render.It renders menu items to the output with the non-selected item with overridden colors when override colors are supplied.`` () =
    let context = 
        Contexts.TestContext()
    (context :> Output.SetPositionContext).PutPosition := Mocks.Expect(1,2)
    (context :> Output.SetBackgroundColorContext).PutBackgroundHue := Mocks.Expect Green
    (context :> Output.SetForegroundColorContext).PutForegroundHue := Mocks.Expect Blue
    (context :> Output.WriteTextContext).PutText := Mocks.Expect "text"
    MenuItem.Render
        context
        (Map.empty |> Map.add true Blue |> Map.add false Green)
        [
            MenuItem.Create((1,2), "value", "text")
        ]
        ""
