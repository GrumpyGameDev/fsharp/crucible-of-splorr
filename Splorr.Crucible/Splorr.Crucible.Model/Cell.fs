﻿namespace Splorr.Crucible.Model

type Cell =
    {
        terrain : Terrain
        actor : Actor option
    }

