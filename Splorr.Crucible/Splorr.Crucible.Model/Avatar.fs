﻿namespace Splorr.Crucible.Model

open System

type AvatarIdentifier = Guid

type Avatar =
    {
        position : Location
    }

