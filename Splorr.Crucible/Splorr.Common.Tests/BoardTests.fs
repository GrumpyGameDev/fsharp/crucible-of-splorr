module Splorr.Common.BoardTests

open System
open Xunit

[<Fact>]
let ``Constructor.It sets Columns and Rows.`` () =
    let subject = Board<int>(2,3,0)
    Assert.Equal(2, subject.Columns)
    Assert.Equal(3, subject.Rows)

[<Theory>]
[<InlineData(0,0)>]
[<InlineData(1,0)>]
[<InlineData(0,2)>]
[<InlineData(1,2)>]
let ``GetCell.It gets the value of a cell within the board.`` (column:int, row:int) =
    let subject = Board<int>(2,3,0)
    let actual = subject.GetCell(column,row)
    Assert.Equal(Some 0, actual)

[<Theory>]
[<InlineData(-1,-1)>]
[<InlineData(-1,0)>]
[<InlineData(0,-1)>]
[<InlineData(2,0)>]
[<InlineData(2,3)>]
[<InlineData(0,3)>]
[<InlineData(3,4)>]
let ``GetCell.It gets nothing when asked for a cell outside the board.`` (column:int, row:int) =
    let subject = Board<int>(2,3,0)
    let actual = subject.GetCell(column, row)
    Assert.Equal(None, actual)

[<Theory>]
[<InlineData(0,0,1)>]
let ``SetCell.It replaces cell contents when given a valid location.`` (column:int, row:int, value:int) =
    let subject = Board<int>(2,3,0)
    subject.SetCell(column, row, value)
    let actual = subject.GetCell(column, row)
    Assert.Equal(Some value, actual)
